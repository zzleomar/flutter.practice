
import 'package:flutter/material.dart';


mixin fonts {
  final h3 = const TextStyle(fontSize: 16.0);
  final h2 = const TextStyle(fontSize: 18.0);
  final h1 = const TextStyle(fontSize: 24.0);
}