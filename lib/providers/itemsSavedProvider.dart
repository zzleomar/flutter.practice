import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:practica/models/Item.dart';

class ItemsSavedProvider with ChangeNotifier {
  var _saved = List<Item>();
  List<Item> get saved => _saved;

  void push(Item item) {
    print('add $item');
    _saved.add(item);
    notifyListeners();
  }

  void remove(int index) async {
    print('remove index $index');
    // _saved.removeWhere((item) => item.id == data.id);
    _saved.removeAt(index);
    notifyListeners();
  }

  int search(var id) {
    var response = _saved.map((item) => item.id).toList().indexOf(id);
    return response;
  }
}