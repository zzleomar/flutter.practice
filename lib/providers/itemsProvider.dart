import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:practica/models/Item.dart';

class ItemsProviders with ChangeNotifier {
  var _listItem;
  get listItem => _listItem;
  set listItem(data) {
    _listItem = data;
    notifyListeners();
  }

  Future<List<dynamic>> loadData() async {
    var listItem = fetchItems();
    _listItem = listItem;
    // notifyListeners();
    return listItem;
  }
}