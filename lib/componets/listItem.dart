import 'package:flutter/material.dart';
import 'package:practica/models/Item.dart';
import 'package:practica/providers/itemsProvider.dart';
import 'package:practica/providers/itemsSavedProvider.dart';
import 'package:provider/provider.dart';
import 'item.dart';
import 'listItemRk.dart';
import 'listSaved.dart';

class ListItemWidget extends StatefulWidget {
  ListItemWidget({Key key}) : super(key: key);

  @override
  _ListItemWidgetState createState() => _ListItemWidgetState();
}

class _ListItemWidgetState extends State<ListItemWidget> {
  @override
  Widget build(BuildContext context) {
    var itemsProviders = Provider.of<ItemsProviders>(context, listen: false);
    Widget lista = Container(
      child: FutureBuilder(
        future: itemsProviders.loadData(),
        // future: fetchItems(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(child: Text('Sin datos'));
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            default:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return itemsBuild(snapshot.data);
          }
        },
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome To My app'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.shopping_cart), onPressed: handleRedirectCart),
          IconButton(icon: Icon(Icons.list), onPressed: handleList)
        ],
      ),
      body: lista,
    );
  }

  itemsBuild(data) {
    return Consumer<ItemsSavedProvider>(builder: (context, provider, __) {
      return CustomScrollView(slivers: <Widget>[
        SliverAppBar(
          pinned: false,
          expandedHeight: MediaQuery.of(context).size.height * 0.40,
          flexibleSpace: FlexibleSpaceBar(
            title: Text('LIST OF ITEMS'),
          ),
        ),
        SliverGrid(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200, //max of height of the view
            mainAxisSpacing: 10.0, //spacing bottom
            crossAxisSpacing: 10.0, //spacing left-rigth
            childAspectRatio: .7, //expressed as a ratio of width to height
          ),
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return itemBuild(data[index], index);
            },
            childCount: data.length,
          ),
        )
      ]);
    });
  }

  void handleRedirectCart() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return ListSavedWidget();
    }));
  }

  void handleList() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return ListItemRkWidget();
    }));
  }

  handleAddSaved(Item newData) {
    final itemsSavedProvider =
        Provider.of<ItemsSavedProvider>(context, listen: false);
    int index = itemsSavedProvider.search(newData.id);
    if (index != -1) {
      itemsSavedProvider.remove(index);
    } else {
      itemsSavedProvider.push(newData);
    }
  }

  Widget itemBuild(item, index) {
    final itemsProviders =
        Provider.of<ItemsSavedProvider>(context, listen: false);
    final alreadySaved = itemsProviders.search(item.id) != -1;
    return Card(
        child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: () {
              handleAddSaved(item);
            },
            child: Container(
              alignment: Alignment.center,
              color: alreadySaved ? Colors.red : Colors.teal[100 * (index % 9)],
              child: ItemWidget(text: item.name),
            )));
  }
}
