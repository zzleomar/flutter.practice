import 'package:flutter/material.dart';
import 'package:practica/models/Item.dart';
import 'item.dart';
import 'listSaved.dart';
// import 'dart:async';

class ListItemRkWidget extends StatefulWidget {
  ListItemRkWidget({Key key}) : super(key: key);

  @override
  _ListItemRkWidgetState createState() => _ListItemRkWidgetState();
}

class _ListItemRkWidgetState extends State<ListItemRkWidget> {
  var listItem;
  void initState() {
    super.initState();
    loadItems();
  }

  void loadItems() {
    setState(() {
      print('cargando');
      listItem = fetchItems();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Welcome To My app'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.shopping_cart), onPressed: handleRedirectCart)
          ],
        ),
        body: Container(
          child: FutureBuilder(
            future: listItem,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Center(child: Text('Sin datos'));
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                default:
                  if (snapshot.hasError)
                    return new Text('Error: ${snapshot.error}');
                  else
                    // return Center( child: Text('Load Complited'));
                    return itemsBuild(snapshot.data);
              }
            },
          ),
        ));
  }

  void handleRedirectCart() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return ListSavedWidget();
    }));
  }

  itemsBuild(data) {
    return CustomScrollView(slivers: <Widget>[
      SliverGrid(
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200, //max of height of the view
          mainAxisSpacing: 10.0, //spacing bottom
          crossAxisSpacing: 10.0, //spacing left-rigth
          childAspectRatio: 0.7, //expressed as a ratio of width to height
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return Container(
              alignment: Alignment.center,
              color: Color.fromRGBO(66, 55, 44, 0.4),
              child: ItemWidget(text: data[index].name),
            );
          },
          childCount: data.length,
        ),
      )
    ]);
  }

  buildItems(data) {
    final listado = data.map<Widget>((f) {
      return Container(
        alignment: Alignment.center,
        color: Color.fromRGBO(38, 38, 38, 0.4),
        child: ItemWidget(text: f.name),
      );
    }).toList();
    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 0.7,
      padding: const EdgeInsets.all(4.0),
      mainAxisSpacing: 10.0,
      crossAxisSpacing: 10.0,
      children: listado,
    );
  }
}
