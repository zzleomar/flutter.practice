import 'package:flutter/material.dart';
// import 'package:practica/styles/fonts.dart';

class ItemWidget extends StatefulWidget {
  final String text;
  ItemWidget({Key key, @required this.text}) : super(key: key);

  @override
  _ItemWidgetState createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Text(widget.text, style: const TextStyle(fontSize: 16.0)),
    );
  }
}
