import 'package:flutter/material.dart';
import 'package:practica/componets/item.dart';
import 'package:practica/models/Item.dart';
import 'package:practica/providers/itemsProvider.dart';
import 'package:practica/providers/itemsSavedProvider.dart';
import 'package:provider/provider.dart';

class ListSavedWidget extends StatefulWidget {
  ListSavedWidget({Key key}) : super(key: key);

  @override
  _ListSavedWidgetState createState() => _ListSavedWidgetState();
}

class _ListSavedWidgetState extends State<ListSavedWidget> {
  @override
  Widget build(BuildContext context) {
    Widget lista = GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 0.7,
        padding: const EdgeInsets.all(4.0),
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        children: itemsBuild());
    return Consumer<ItemsSavedProvider>(builder: (context, provider, __) {
      return Scaffold(
          appBar: AppBar(title: Text('List of items saved')),
          body: lista,
        );
      });
  }

  itemsBuild() {
    var itemsProviders = Provider.of<ItemsSavedProvider>(context);
    final listado = itemsProviders.saved.map<Widget>((f) {
      return Card(
          child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {
                handleRemoveSaved(f);
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.red,
                child: ItemWidget(text: f.name),
              )));
    });
    return listado.toList();
  }

  handleRemoveSaved(Item newData) {
    final itemsSavedProvider =
        Provider.of<ItemsSavedProvider>(context, listen: false);
    int index = itemsSavedProvider.search(newData.id);
    if (index != -1) {
      itemsSavedProvider.remove(index);
    }
  }
}
