import 'package:flutter/material.dart';
import 'package:practica/componets/listItem.dart';
import 'package:practica/providers/itemsSavedProvider.dart';
import 'package:provider/provider.dart';

import 'providers/itemsProvider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ItemsProviders()),
          ChangeNotifierProvider(create: (context) => ItemsSavedProvider()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'My App',
          home: ListItemWidget(),
        ));
  }
}
