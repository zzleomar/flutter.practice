import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:http/http.dart';

class Item {
  final dynamic id;
  final dynamic name;
  final dynamic description;
  final dynamic price;
  final dynamic photo;
  final dynamic enabled;
  final dynamic categoryId;
  final dynamic extraId;
  final dynamic size;

  Item(
      {this.id,
      this.name,
      this.description,
      this.price,
      this.photo,
      this.enabled,
      this.categoryId,
      this.extraId,
      this.size});

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        price: json['price'],
        photo: json['photo'],
        enabled: json['enabled'],
        categoryId: json['category_id'],
        extraId: json['extra_id'],
        size: json['size']);
  }
}

Future<List<dynamic>> fetchItems() async {
  var url = 'http://192.168.43.164:5000/item/';
  Response response = await http.get(url);
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    final data = jsonResponse['data'].map((element) => Item.fromJson(element)).toList();
    return data;
  } else {
    throw Exception('Failed to load items');
  }
}
