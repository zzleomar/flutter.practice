import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

void main(List<String> arguments) async {
  var url = 'http://localhost:5000/item/';
  var response = await http.get(url);
  print('response ${response.statusCode}');
  if (response.statusCode == 200) {
    print(response.body);
    var jsonResponse = convert.jsonDecode(response.body);
    // var jsonResponse = json.decode(response.body);
    print(jsonResponse['data']);
    // return jsonResponse['data'].map((element) => Item.fromJson(element)).toList();
  } else {
    throw Exception('Failed to load items');
  }
}